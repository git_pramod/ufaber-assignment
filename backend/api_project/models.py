from django.db import models
from backend.api_accounts.models import BaseModel
from django.contrib.auth.models import User
import uuid
from django.utils.translation import ugettext as _


class Project(BaseModel):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(_('name'), max_length=200, unique=True)
    description = models.TextField()
    date_of_completion = models.DateField()
    avatar = models.ImageField(upload_to='avatar/', null=True, blank=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='project_owner')

    def __str__(self):
        return self.name
