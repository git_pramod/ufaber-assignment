from django.views.generic import TemplateView
from ufaber_assignment.settings import DOMAIN
import requests
from django.shortcuts import reverse, redirect
from backend.api_task.models import PENDING, IN_PROCESS
from django.contrib import messages
from frontend.utils import process_json_response


class ProjectListView(TemplateView):
    template_name = 'project/projects.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)

        # projects API call
        project_url = '{0}/api/project/project-crud/'.format(DOMAIN)
        headers = {'Authorization': 'Bearer {0}'.format(request.session.get('access_token'))}
        project_response = requests.get(url=project_url, headers=headers)
        if project_response.status_code == 200:
            # user init API call
            user_url = '{0}/api/accounts/user-init/'.format(DOMAIN)
            user_response = requests.get(url=user_url, headers=headers)
            if user_response.status_code == 200:
                user_response_json = user_response.json()
                request.session['user_details'] = user_response_json
            else:
                return process_json_response(request=request, response=user_response)

            projects = project_response.json()['results']
            context['projects'] = projects
            if len(projects) == 0:
                return redirect(reverse('project:add-project'))
            return self.render_to_response(context)
        else:
            return process_json_response(request=request, response=project_response)


class AddProjectView(TemplateView):
    template_name = 'project/add-edit-project.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'A'
        return context

    def post(self, request, *args, **kwargs):
        url = '{0}/api/project/project-crud/'.format(DOMAIN)
        headers = {'Authorization': 'Bearer {0}'.format(request.session.get('access_token'))}
        response = requests.post(url=url, data=request.POST, files=request.FILES, headers=headers)
        if response.status_code == 201:
            messages.success(request, 'Project added successfully')
            return redirect(reverse('project:dashboard', kwargs={'pk': response.json()['id']}))
        else:
            return process_json_response(request=request, response=response)


class EditProjectView(TemplateView):
    template_name = 'project/add-edit-project.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'E'
        return context

    def get(self, request, *args, **kwargs):
        project_id = kwargs.get('pk')
        context = self.get_context_data(**kwargs)
        url = '{0}/api/project/project-crud/{1}/'.format(DOMAIN, project_id)
        headers = {'Authorization': 'Bearer {0}'.format(request.session.get('access_token'))}
        response = requests.get(url=url, headers=headers)
        if response.status_code == 200:
            project = response.json()
            context['project'] = project
        else:
            return process_json_response(request=request, response=response)
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        request.POST._mutable = True
        if 'avatar' in request.POST.keys():
            request.POST.pop('avatar')
        project_id = kwargs.get('pk')
        url = '{0}/api/project/project-crud/{1}/'.format(DOMAIN, project_id)
        headers = {'Authorization': 'Bearer {0}'.format(request.session.get('access_token'))}
        response = requests.patch(url=url, data=request.POST, files=request.FILES, headers=headers)
        if response.status_code == 200:
            messages.success(request, 'Project edited successfully')
            return redirect(reverse('project:all-projects'))
        else:
            return process_json_response(request=request, response=response)


class DashboardView(TemplateView):
    template_name = 'project/dashboard.html'

    def get(self, request, *args, **kwargs):
        project_id = kwargs.get('pk')
        request.session['project_id'] = project_id
        context = self.get_context_data(**kwargs)
        url = '{0}/api/project/dashboard-api/?project_id={1}'.format(DOMAIN, project_id)
        headers = {'Authorization': 'Bearer {0}'.format(request.session.get('access_token'))}
        response = requests.get(url=url, headers=headers)
        if response.status_code == 200:
            response_json = response.json()
            task_status_data = response_json['task_status_data'][0] if response_json['task_status_data'] else []
            project_data = response_json['project_data']

            context['task_status_data'] = task_status_data
            context['project_data'] = project_data
            context['tasks_data'] = response_json['tasks_data']
            context['PENDING'] = PENDING
            context['IN_PROCESS'] = IN_PROCESS
            request.session['project_name'] = project_data['name']
        else:
            return process_json_response(request=request, response=response)
        return self.render_to_response(context)
