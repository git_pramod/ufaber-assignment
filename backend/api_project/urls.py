from backend.api_project.views import ProjectViewSet, DashboardAPIView
from rest_framework import routers
from django.urls import path


router = routers.SimpleRouter()
router.register('project-crud', ProjectViewSet)

urlpatterns = router.urls

urlpatterns += [
    path('dashboard-api/', DashboardAPIView.as_view(), name='dashboard'),
]