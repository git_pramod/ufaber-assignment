from django.urls import resolve
from ufaber_assignment.settings import LOGIN_REQUIRED_URLS
import re
from django.contrib.auth.decorators import login_required
from django.utils.deprecation import MiddlewareMixin


class LoginRequiredMiddleware(MiddlewareMixin):

    def __init__(self, get_response):
        self.required = tuple(re.compile(url) for url in LOGIN_REQUIRED_URLS)
        self.get_response = get_response

    def process_view(self, request, view_func, view_args, view_kwargs):
        if request.session.get('access_token'):
            return None
        for url in self.required:
            if url.match(request.path):
                return login_required(view_func, login_url='/accounts/login/')(request, *view_args, **view_kwargs)
        return None
