from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView
from django.conf.urls.static import static
from ufaber_assignment.settings import MEDIA_ROOT, MEDIA_URL
from rest_framework_simplejwt import views as jwt_views

urlpatterns = [
    # admin URLs
    path('admin/', admin.site.urls),

    # Login URL
    path('', TemplateView.as_view(template_name='accounts/login.html'), name='login'),

    # other include URLs
    path('accounts/', include('frontend.accounts.urls'), name='accounts'),
    path('project/', include('frontend.project.urls'), name='project'),
    path('task/', include('frontend.task.urls'), name='task'),

    path('api/accounts/', include('backend.api_accounts.urls'), name='api-accounts'),
    path('api/project/', include('backend.api_project.urls'), name='api-project'),
    path('api/task/', include('backend.api_task.urls'), name='api-task'),


    path('api/token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
]

urlpatterns += static(MEDIA_URL, document_root=MEDIA_ROOT)