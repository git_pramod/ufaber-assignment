from rest_framework.viewsets import ModelViewSet
from rest_framework.generics import CreateAPIView
from django.contrib.auth.models import User
from backend.api_accounts.serializers import UserSerializer, UserDetailSerializer
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView
from rest_framework.status import HTTP_200_OK
from rest_framework.response import Response


class UserCreateAPIView(CreateAPIView):
    model = User
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = [AllowAny]
    authentication_classes = []


class UserViewSet(ModelViewSet):
    model = User
    queryset = User.objects.all()
    serializer_class = UserDetailSerializer


class UserInitView(APIView):

    def get(self, request, *args, **kwargs):
        user_data = UserDetailSerializer(request.user).data
        return Response(status=HTTP_200_OK, data=user_data)
