from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView
from backend.api_project.models import Project
from backend.api_task.models import Task
from backend.api_project.serializers import ProjectSerializer, ProjectDetailSerializer
from backend.api_task.serializers import TaskSerializer, TaskReportSerializer
from django.db.models import Count, F, Q, Sum, When, Case, IntegerField
from backend.constants import PENDING, DONE, IN_PROCESS, TASK
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK


class ProjectViewSet(ModelViewSet):
    model = Project
    queryset = Project.objects.all().order_by('name')
    
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return ProjectDetailSerializer
        else:
            return ProjectSerializer

    def list(self, request, *args, **kwargs):
        self.queryset = self.get_queryset().filter(
            Q(owner=self.request.user) | Q(tasks__assignee=self.request.user)
        ).distinct()
        return super().list(request, *args, **kwargs)


class DashboardAPIView(APIView):

    def get(self, request, *args, **kwargs):
        project_id = request.GET.get('project_id')

        # task status report
        task_report = Task.objects.select_related('assignee').filter(
            project=project_id, assignee=request.user
        ).values(first_name=F('assignee__first_name'), last_name=F('assignee__last_name')).annotate(
            total_tasks=Count('assignee__username'),
            total_in_process=Sum(Case(
                When(status=IN_PROCESS, then=1),
                default=0, output_field=IntegerField()
            )),
            total_pending=Sum(Case(
                When(status=PENDING, then=1),
                default=0, output_field=IntegerField()
            )),
            total_completed=Sum(Case(
                When(status=DONE, then=1),
                default=0, output_field=IntegerField()
            ))
        )
        task_status_data = TaskReportSerializer(task_report, many=True).data

        # project data
        project = Project.objects.get(id=project_id)
        project_data = ProjectSerializer(project).data

        # recently added tasks
        tasks = Task.objects.filter(
            project=project_id, assignee=request.user, task_type=TASK, status__in=[PENDING, IN_PROCESS]
        ).order_by('-created_at')[0:5]
        tasks_data = TaskSerializer(tasks, many=True).data

        final_data = {
            'task_status_data': task_status_data, 'project_data': project_data, 'tasks_data': tasks_data
        }

        return Response(status=HTTP_200_OK, data=final_data)
