from django.shortcuts import redirect, reverse
from django.views.generic import TemplateView
from ufaber_assignment.settings import DOMAIN
import requests
from django.contrib import messages
from django.contrib.auth import logout
from frontend.utils import process_json_response


class RegisterView(TemplateView):
    template_name = 'accounts/register.html'

    def post(self, request, *args, **kwargs):
        url = '{0}/api/accounts/user-create/'.format(DOMAIN)
        response = requests.post(url=url, data=request.POST)
        if response.status_code == 201:
            messages.success(request, 'Registered successfully')
            return redirect(reverse('accounts:login'))
        else:
            return process_json_response(request=request, response=response)


class LoginView(TemplateView):
    template_name = 'accounts/login.html'

    def post(self, request, *args, **kwargs):
        url = '{0}/api/token/'.format(DOMAIN)
        response = requests.post(url=url, data=request.POST)
        if response.status_code == 200:
            request.session['access_token'] = response.json()['access']
            return redirect(reverse('project:all-projects'))
        else:
            messages.error(request, response.json()['detail'])
            return redirect(reverse('accounts:login'))


class LogOutView(TemplateView):
    template_name = 'accounts/logout.html'

    def get(self, request, *args, **kwargs):
        logout(request=request)
        return super().get(request, *args, **kwargs)
