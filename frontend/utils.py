from django.http import JsonResponse, Http404
from backend.constants import TASK, SUB_TASK
from django.contrib import messages
from django.shortcuts import redirect, reverse
from django.http import HttpResponse


def process_json_response(request, response):
    if response.status_code == 400:
        errors = response.json()
        if 'detail' in errors:
            message = errors['detail']
        elif 'non_field_errors' in errors:
            message = errors['non_field_errors']
        else:
            try:
                message = errors[0]
            except:
                message = errors
        messages.error(request, message)
        return redirect(request.path_info)

    elif response.status_code == 401:
        return redirect(reverse('accounts:logout'))

    elif response.status_code == 404:
        return HttpResponse('<h1>404 - Page Not Found</h1>')

    elif response.status_code == 500:
        return HttpResponse('<h1>500 - Internal Server Error</h1>')


def common_context_for_task(request, context, request_type):
    context['type'] = request_type
    task_type = int(request.GET.get('tt')) if request.GET.get('tt') else TASK
    task_type = task_type if task_type in [TASK, SUB_TASK] else None
    if not task_type:
        raise Http404

    context['task_type'] = task_type if task_type in [TASK, SUB_TASK] else TASK
    context['TASK'] = TASK
    context['SUB_TASK'] = SUB_TASK
    return context
