from django import template
import datetime, re

register = template.Library()


@register.filter
def date_format(input_date):
    if not input_date:
        return ''
    return datetime.datetime.strptime(input_date, '%Y-%m-%d').strftime('%d %b %Y')


@register.filter
def replace_special_chars_with_hyphen(string_text):
    string = re.sub('[^a-z0-9\s]', '-', string_text.lower())
    return re.sub('[_\s]', '-', string)
