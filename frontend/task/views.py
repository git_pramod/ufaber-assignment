from django.views.generic import TemplateView
from ufaber_assignment.settings import DOMAIN
import requests
from django.shortcuts import reverse, redirect
from backend.constants import TASK, SUB_TASK
from django.http import JsonResponse, Http404
from django.views import View
from django.contrib import messages
from frontend.utils import common_context_for_task, process_json_response


class TaskListView(TemplateView):
    template_name = 'task/tasks.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        context['TASK'] = TASK
        if 'task' in request.GET:
            path = self.request.get_full_path()
            split_url = path.split('?')
            query_params = split_url[1]

            url = '{0}/api/task/task-list/'.format(DOMAIN)
            headers = {'Authorization': 'Bearer {0}'.format(request.session.get('access_token'))}
            response = requests.get(url=url, headers=headers, params=query_params)
            if response.status_code == 200:
                return JsonResponse(data=response.json())
            else:
                return process_json_response(request=request, response=response)
        return self.render_to_response(context)


class SubTaskListView(TemplateView):
    template_name = 'task/sub-tasks.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        context['TASK'] = TASK
        context['SUB_TASK'] = SUB_TASK
        request.session['current_task_id'] = kwargs.get('task_id')
        request.session['current_task_title'] = kwargs.get('task_title')

        if 'sub-task' in request.GET:
            path = self.request.get_full_path()
            split_url = path.split('?')
            query_params = split_url[1]

            url = '{0}/api/task/task-list/'.format(DOMAIN)
            headers = {'Authorization': 'Bearer {0}'.format(request.session.get('access_token'))}
            response = requests.get(url=url, headers=headers, params=query_params)
            if response.status_code == 200:
                return JsonResponse(data=response.json())
            else:
                return process_json_response(request=request, response=response)
        return self.render_to_response(context)


class AddTaskView(TemplateView):
    template_name = 'task/add-edit-task.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context = common_context_for_task(request=self.request, context=context, request_type='A')
        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        url = '{0}/api/accounts/user/'.format(DOMAIN)
        headers = {'Authorization': 'Bearer {0}'.format(request.session.get('access_token'))}
        response = requests.get(url=url, headers=headers)
        if response.status_code == 200:
            user_list = response.json()['results']
            context['user_list'] = user_list
        else:
            return process_json_response(request=request, response=response)
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        task_type = int(request.POST.get('task_type')) if request.POST.get('task_type') else TASK
        if task_type == SUB_TASK:
            success_url = reverse('task:sub-tasks', kwargs={
                'task_id': request.session.get('current_task_id'),
                'task_title': request.session.get('current_task_title')
            })
            success_message = 'Sub Task added successfully'
        else:
            success_url = reverse('task:all-tasks')
            success_message = 'Task Added successfully'
        url = '{0}/api/task/task-crud/'.format(DOMAIN)
        headers = {'Authorization': 'Bearer {0}'.format(request.session.get('access_token'))}
        response = requests.post(url=url, data=request.POST, headers=headers)
        if response.status_code == 201:
            messages.success(request, success_message)
            return redirect(success_url)
        else:
            return process_json_response(request=request, response=response)


class EditTaskView(TemplateView):
    template_name = 'task/add-edit-task.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context = common_context_for_task(request=self.request, context=context, request_type='E')
        return context

    def get(self, request, *args, **kwargs):
        task_id = kwargs.get('pk')
        context = self.get_context_data(**kwargs)
        url = '{0}/api/task/task-crud/{1}/'.format(DOMAIN, task_id)
        headers = {'Authorization': 'Bearer {0}'.format(request.session.get('access_token'))}
        response = requests.get(url=url, headers=headers)
        if response.status_code == 200:
            task = response.json()
            context['task'] = task
        else:
            return process_json_response(request=request, response=response)

        # get users list
        url = '{0}/api/accounts/user/'.format(DOMAIN)
        headers = {'Authorization': 'Bearer {0}'.format(request.session.get('access_token'))}
        response = requests.get(url=url, headers=headers)
        if response.status_code == 200:
            user_list = response.json()['results']
            context['user_list'] = user_list
        else:
            return process_json_response(request=request, response=response)
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        task_id = kwargs.get('pk')
        task_type = int(request.POST.get('task_type')) if request.POST.get('task_type') else TASK
        if task_type == SUB_TASK:
            success_url = reverse('task:sub-tasks', kwargs={
                'task_id': request.session.get('current_task_id'),
                'task_title': request.session.get('current_task_title')
            })
            success_message = 'Sub Task edited successfully'
        else:
            success_url = reverse('task:all-tasks')
            success_message = 'Task edited successfully'

        url = '{0}/api/task/task-crud/{1}/'.format(DOMAIN, task_id)
        headers = {'Authorization': 'Bearer {0}'.format(request.session.get('access_token'))}
        response = requests.patch(url=url, data=request.POST, headers=headers)

        if response.status_code == 200:
            messages.success(request, success_message)
            return redirect(success_url)
        else:
            return process_json_response(request=request, response=response)


class DeleteTaskView(View):

    def post(self, request, *args, **kwargs):
        task_id = kwargs.get('pk')
        task_type = int(request.POST.get('task_type')) if request.POST.get('task_type') else None
        task_type = task_type if task_type in [TASK, SUB_TASK] else None
        if not task_type:
            raise Http404

        if task_type == SUB_TASK:
            redirect_url = reverse('task:sub-tasks', kwargs={
                'task_id': request.session.get('current_task_id'),
                'task_title': request.session.get('current_task_title')
            })
            success_message = 'Sub Task deleted successfully'
        else:
            redirect_url = reverse('task:all-tasks')
            success_message = 'Task deleted successfully'

        url = '{0}/api/task/task-crud/{1}/'.format(DOMAIN, task_id)
        headers = {'Authorization': 'Bearer {0}'.format(request.session.get('access_token'))}
        response = requests.delete(url=url, headers=headers)
        if response.status_code == 204:
            messages.success(request, success_message)
            return redirect(redirect_url)
        else:
            return process_json_response(request=request, response=response)
