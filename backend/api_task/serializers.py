from rest_framework import serializers
from backend.api_task.models import Task
from backend.api_accounts.serializers import UserSerializer
from backend.api_project.serializers import ProjectSerializer


class TaskSerializer(serializers.ModelSerializer):

    class Meta:
        model = Task
        exclude = ('is_active',)


class TaskDetailSerializer(serializers.ModelSerializer):
    assignee = UserSerializer()
    parent_task = TaskSerializer()
    project = ProjectSerializer()

    class Meta:
        model = Task
        fields = '__all__'


class TaskReportSerializer(serializers.Serializer):
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    total_tasks = serializers.IntegerField()
    total_in_process = serializers.IntegerField()
    total_pending = serializers.IntegerField()
    total_completed = serializers.IntegerField()
