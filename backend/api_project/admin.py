from django.contrib import admin
from backend.api_project.models import Project

admin.site.register(Project)
