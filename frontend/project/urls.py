from django.urls import path, re_path
from frontend.project.views import ProjectListView, DashboardView, AddProjectView, EditProjectView

app_name = 'project'

urlpatterns = [
    path('all-projects/', ProjectListView.as_view(), name='all-projects'),
    path('add-project/', AddProjectView.as_view(), name='add-project'),
    re_path(r'^edit-project/(?P<pk>[0-9a-f-]+)/$', EditProjectView.as_view(), name='edit-project'),
    re_path(r'^dashboard/(?P<pk>[0-9a-f-]+)/$', DashboardView.as_view(), name='dashboard'),
]
