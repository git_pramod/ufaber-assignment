from django.urls import path, re_path
from frontend.task.views import TaskListView, AddTaskView, EditTaskView, SubTaskListView, DeleteTaskView

app_name = 'task'

urlpatterns = [
    path('all-tasks/', TaskListView.as_view(), name='all-tasks'),
    re_path(r'^sub-tasks/(?P<task_id>[0-9a-f-]+)/(?P<task_title>(.*))/$', SubTaskListView.as_view(), name='sub-tasks'),
    path('add-task/', AddTaskView.as_view(), name='add-task'),
    re_path(r'^project/(?P<project_id>[0-9a-f-]+)/edit-task/(?P<pk>[0-9a-f-]+)/$', EditTaskView.as_view(), name='edit-task'),
    re_path(r'^delete/(?P<pk>[0-9a-f-]+)/$', DeleteTaskView.as_view(), name='delete-task'),
]
