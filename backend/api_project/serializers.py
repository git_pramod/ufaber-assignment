from rest_framework import serializers
from backend.api_project.models import Project
from backend.api_accounts.serializers import UserSerializer


class ProjectSerializer(serializers.ModelSerializer):

    class Meta:
        model = Project
        exclude = ('is_active',)


class ProjectDetailSerializer(serializers.ModelSerializer):
    owner = UserSerializer()

    class Meta:
        model = Project
        fields = '__all__'
