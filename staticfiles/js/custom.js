var table, table_data, row_id;

// common form validation function
function common_form_validation_fn(form, rules, form_group_class) {
    form.validate({
        rules: rules,
        errorElement: 'span',
        errorPlacement: function(error, element) {
            error.addClass('invalid-feedback');
            element.closest('.' + form_group_class).append(error);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        },
    });
}

// render datatable function with all the parameters
function render_datatable_function(date_format_col_list) {
    var column_defs = [];
    if (date_format_col_list) {
        column_defs.push (
            {
                "targets":[date_format_col_list[0], date_format_col_list[1]],
                "render":function (data) {
                    if (data) {
                        var date = new Date(data);
                        var locale = 'en';
                        var month = date.toLocaleString(locale, {month: "short"});
                        return date.getDate() + " " + month + " " + date.getFullYear();
                    }
                    else {
                        return ''
                    }
                }
            }
        )
    }

    table = $('#datatable').DataTable({
        "processing": true,
        "serverSide": true,
        "stateSave": true,
        "autoWidth": false,
        "scrollX": true,
        "columnDefs": column_defs,
    });

    table.search('').columns().search('').draw();
    $('#datatable tbody').on('click', 'tr' , function (event) {
        table_data = table.row(this).data();
        row_id = table_data.id;

        var target = $(event.target);
        if (target.is('#edit_button')) {
            edit_entity(table_data, table);
        }
        else if (target.is('#retrieve_button')) {
            retrieve_entity(table_data, table);
        }
        else if (target.is('#delete_button')) {
            delete_entity(row_id, table);
        }
    });
}

// string format function
String.prototype.format = function() {
    var a = this;
    for (var k in arguments) {
        a = a.replace("{" + k + "}", arguments[k])
    }
    return a
};

// common form submit function
function formSubmitCommonFunction(current_selector) {
    if (current_selector.closest('form').valid()) {
        current_selector.attr('disabled', true);
        current_selector.closest('form').submit()
    }
}

$('input').on('keypress', function(e) {
    if (e.which === 13) {
        $(this).closest('form').submit()
    }
});
