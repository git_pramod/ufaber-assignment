from rest_framework.viewsets import ModelViewSet
from rest_framework.generics import ListAPIView
from backend.api_task.models import Task
from backend.api_task.serializers import TaskSerializer, TaskDetailSerializer
from backend.constants import TASK, SUB_TASK


class TaskListAPIView(ListAPIView):
    model = Task
    serializer_class = TaskDetailSerializer

    def get_queryset(self):
        project_id = self.request.GET.get('project_id')
        task_type = self.request.GET.get('task_type')
        task_id = self.request.GET.get('task_id')
        task_type = int(task_type) if task_type else TASK

        if task_type == TASK:
            queryset = Task.objects.filter(task_type=TASK, project_id=project_id, assignee=self.request.user)
        elif task_type == SUB_TASK:
            queryset = Task.objects.filter(parent_task_id=task_id, task_type=SUB_TASK)
        else:
            queryset = Task.objects.none()
        return queryset


class TaskViewSet(ModelViewSet):
    model = Task
    queryset = Task.objects.all()

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return TaskDetailSerializer
        else:
            return TaskSerializer

