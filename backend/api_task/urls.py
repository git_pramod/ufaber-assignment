from backend.api_task.views import TaskViewSet, TaskListAPIView
from rest_framework import routers
from django.urls import path


router = routers.SimpleRouter()
router.register('task-crud', TaskViewSet)

urlpatterns = router.urls

urlpatterns += [
    path('task-list/', TaskListAPIView.as_view(), name='task-list'),
]
