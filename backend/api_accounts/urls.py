from backend.api_accounts.views import UserViewSet, UserCreateAPIView, UserInitView
from rest_framework import routers
from django.urls import path


router = routers.SimpleRouter()
router.register('user', UserViewSet)

urlpatterns = router.urls


urlpatterns += [
    path('user-create/', UserCreateAPIView.as_view(), name='user-create'),
    path('user-init/', UserInitView.as_view(), name='user-init'),
]

