from django.db import models
import uuid


class BaseModel(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created_at = models.DateTimeField(auto_now_add=True,editable=False)
    updated_at = models.DateTimeField(auto_now=True,editable=False)
    created_by = models.ForeignKey(
        "auth.User", on_delete=models.CASCADE, related_name="created_by_%(app_label)s_%(class)s_set",
        null=True, blank=True,editable=False
    )
    modified_by = models.ForeignKey(
        "auth.User", on_delete=models.CASCADE, null=True, blank=True,
        related_name="modified_by_%(app_label)s_%(class)s_set", editable=False
    )
    is_active = models.BooleanField(default=True)

    class Meta:
        abstract = True
