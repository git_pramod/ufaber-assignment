from django.db import models
from backend.api_accounts.models import BaseModel
from django.contrib.auth.models import User
from backend.api_project.models import Project
import uuid
from django.utils.translation import ugettext as _
from backend.constants import TASK, SUB_TASK, PENDING, DONE, IN_PROCESS


class Task(BaseModel):
    TASK_TYPES = [(TASK, 'Task'), (SUB_TASK, 'Sub Task')]
    TASK_STATUS_CHOICES = [(PENDING, 'Pending'), (DONE, 'Done'), (IN_PROCESS, 'In Process')]

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name='tasks')
    assignee = models.ForeignKey(User, on_delete=models.CASCADE)
    parent_task = models.ForeignKey("Task", on_delete=models.CASCADE, null=True, blank=True)
    task_type = models.IntegerField(choices=TASK_TYPES, default=TASK, db_index=True)

    title = models.CharField(_('name'), max_length=200, unique=True)
    description = models.TextField()
    start_date = models.DateField()
    end_date = models.DateField()
    status = models.CharField(max_length=20, choices=TASK_STATUS_CHOICES, default=PENDING)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if self.task_type == TASK:
            self.parent_task_id = self.id
        super().save(*args, **kwargs)
